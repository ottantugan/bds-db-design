CREATE TABLE student(
    student_id BIGSERIAL NOT NULL,
	student_name VARCHAR(20) NOT NULL,
	student_surname VARCHAR(20) NOT NULL,
	student_picture bytea,
	student_birthdate DATE NOT NULL,
	PRIMARY KEY (student_id)
);

CREATE TABLE teacher(
    teacher_id BIGSERIAL NOT NULL,
	teacher_name VARCHAR(20) NOT NULL,
	teacher_surname VARCHAR(20) NOT NULL,
	teacher_picture bytea,
	teacher_birthdate DATE NOT NULL,
	PRIMARY KEY (teacher_id)
);


CREATE TABLE subjects (
    subject_id BIGSERIAL NOT NULL,
	subject_title VARCHAR(20), 
);


CREATE TABLE students_progress(
student_id int REFERENCES student (student_id), 
subject_id int REFERENCES subjects (subject_id),
CONSTRAINT student_progress_pkey PRIMARY KEY (student_id, subject_id)  -- many to many
);


CREATE TABLE contacts (
contact_id INT,
contact_type VARCHAR(20),
contact_value VARCHAR(20),
student_id INT,
teacher_id INT,
PRIMARY KEY (contact_id)
);

CREATE TABLE relationship(
relationship_id INT,
relationship_type VARCHAR(20),
user_id1 INT,
user_id2 INT,
PRIMARY KEY (relationship_id)
);

CREATE TABLE competition(
competition_id INT,
competition_title VARCHAR(20),
competition_date DATE,
competition_description VARCHAR(20)
PRIMARY KEY (competition_id)
);

CREATE TABLE competition_has(
competition_id INT,
student_id INT,
FOREIGN KEY student_id 
REFERENCES public.student (student_id)
FOREIGN KEY competition_id 
REFERENCES public.competition (competition_id)
);

CREATE TABLE meeting (
meeting_id INT,
meeting_title VARCHAR (20),
meeitng_date DATE,
meeting_description VARCHAR(45)
PRIMARY KEY meeting_id
);

CREATE TABLE meeting_has_teacher (
meeting_id INT,
teacher_id INT,
FOREIGN KEY meeting_id 
REFERENCES public.meeting (meeting_id)
FOREIGN KEY teacher_id
REFERENCES public.teacher (teacher_id)
);

CREATE TABLE meeting_has_student (
meeting_id INT,
student_id INT,
FOREIGN KEY meeting_id 
REFERENCES public.meeting (meeting_id)
FOREIGN KEY student_id
REFERENCES public.student (student_id)
);

CREATE TABLE faculty (
faculty_id INT,
faculty_title VARCHAR(20)
PRIMARY KEY (faculty_id)
);

CREATE TABLE student_role (
student_id INT,
faculty_id INT,
year INT,
FOREIGN KEY faculty_id
REFERENCES public.faculty (faculty_id)
);

CREATE TABLE staff (
    person_id int not null,
	person_surname VARCHAR(20) not null,
	person_name VARCHAR(20) not null,
	role_title VARCHAR(20) not null,
	PRIMARY KEY (person_id)
);





